import os, shutil

pwd = os.getcwd()
f = open('mvhistory.txt','a')

for item in os.listdir(pwd):
    if os.path.isdir(item):
        f.write(item+'\n')
        os.chdir('..')
        if os.path.exists(item):
            shutil.copy(os.path.join(pwd, item, 'index.html'), item)
        else:
            os.makedirs(item)
            shutil.copy(os.path.join(pwd, item, 'index.html'), item)
    os.chdir(pwd)
f.close()

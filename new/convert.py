import os, shutil

pwd = os.getcwd()

for item in os.listdir(pwd):
    if '.html' in item:
        folder = item.replace('_', '')
        folder = folder.replace('4s', 's')
        folder = folder.replace('.html', '')
        #folder = folder.replace('cluster1h', '')
        os.makedirs(folder)
        shutil.copy(item, folder)
        os.chdir(folder)
        os.rename(item, 'index.html')
    os.chdir(pwd)